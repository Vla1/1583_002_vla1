import java.util.*;
public class CombatCalculator2{

	private static int heroAP;
    private static int heroHealth;
    private static int magicPow;
    private static int points=30;//used as money to level him up
    private static String[] monNameArray= {"Golblin","Orc","Troll"};
    private static int[] monHealthArray= new int[3];
    private static int[] monXpArray= new int[3];
    private static int[] monApArray= new int[3];
    private static int monNumber;//index of monster array
    private static final int HP = 10;
    private static final int AP = 1;
    private static final int MP = 3;
    
	public static void main(String[] args){
		boolean isCombat =true;
		while(isCombat){
			createHero();
			monNumber = createMonster();
			isCombat = runCombat(monNumber);
		}

	}

	public static boolean runCombat(int monNumber){
		  boolean combat = true;
	        while(combat){
			Scanner input = new Scanner(System.in);
			reportStatus(monNumber);
			/*Combat menu Prompt*/
	            //Print Option 1: Sword Attack
	            //Printoptions 2: Cast Spell
	            //Print Option 3: Charge Mana
	            //Print Option 4: Run Away
	            //Prompt Player for action
	        
	            System.out.println("Combat Option:");
	            System.out.println(" 1) Sword Attack");
	            System.out.println(" 2) Cast Spell");
	            System.out.println(" 3) Charge Mana:");
	            System.out.println(" 4) Run Away");
	            System.out.print("What action do you want to perform? ");
	        
	            //Declare variable for user input (as number) and acquire value from Scanner object
		    	int option = input.nextInt();
	        
	            //If player choose option 1,(check with equality operator)
	                //print attack text:
	                //“You strike the<monster name>with your word for<hero attack>damage”
	                //Calculate damage & update monster health using subtraction
	                //Calculation: new monster health is old monster health minus hero attack power
	            if(option == 1){
	                System.out.println("\nYou strike the " + monNameArray[monNumber] +" with your word for " + heroAP +" damage\n");
	                monHealthArray[monNumber] -= heroAP;
	            }
	        
	            //If player choose option 2,(check with equality operator)
	                //print spell message:
	                //“You cast the weaken spell on the monster.”
	                //Calculate damage & update monster health using division
	                //Calculation: new monster health is old monster divided by two
	            else if(option == 2){
	                //If Player Has 3 Or More Magic Points
	                if(magicPow >= 3){
	                    //Reduce players mana points by the spell cast using subtraction
	                    //Calculation:new magic power is old magic power minus 3
	                    magicPow -= 3;
	                    System.out.println("\nYou cast the weaken spell on the monster\n");
	                    monHealthArray[monNumber] /= 2;
	                }
	                else
	                    System.out.println("\nYou don't have enough mana\n");
	                continue;
	            }
	        
	            //Else if the player chose option 3,(check with equality operator)
	                //print charging message:
	                //"You focus and charge your magic power."
	                //Increment magic points and update players magic using addition
	                //Calculation: new hero magic is old hero magic plus one
	            else if(option == 3){
	                System.out.println("\nYou focus and charge your magic power\n");
	                magicPow += 1;
	
	            }
	        
	            //Else if the player chose option 3,(check with equality operator)
	                //Stop Combat Loop By setting control variable to false
	                //print retreat message:
	                //"You run away!"
	            else if(option == 4){
	                System.out.println("\nYou run away!\n");
	                combat = false;
	                continue;
	            }
	        
	            //Else the player chose incorrectly
	                //prints error message:
	                //"I Don't understand that Command."
	            else{
	                System.out.println("\nI don't understand that command\n");
	                continue;
	            }
	            System.out.printf("%s attacks you back with %d points", monNameArray[monNumber], monApArray[monNumber]);
	            heroHealth -= monApArray[monNumber];
	            //If Monsters health is 0 or below
	                //Stop combat loop by setting control variable to false
	                //Print Victory message:“You defeated the <monster name>!”
	            if(monHealthArray[monNumber] <= 0){
	                combat = false;
	                System.out.println("\nYou defeated the "+monNameArray[monNumber]+"and receive "+monXpArray[monNumber]+" xp points\n");
	                System.out.println();
	                points += monXpArray[monNumber];
	               
	            }
	            if(heroHealth <=0){
	            	System.out.printf("You are defeated by %s", monNameArray[monNumber]);
	            	System.out.println("Game over!!!");
	            	System.out.println();
	            	return false;
	            }

	        }
	        return true;
    }
	public static void printHeroStatus(){
        System.out.printf("Hero HP: %d", heroHealth);
	    System.out.printf(", AP: %d",heroAP);
	    System.out.printf(", MP: %d",magicPow);
	    System.out.printf(", money: %d coins",points);
	    System.out.println();
    }

	public static void createHero(){
	    //upgrade the Hero status by the Hero's points
	    //ask the player to choose the type of hero

	    Scanner input = new Scanner(System.in);	    
	    while(points >0 ){
		    printHeroStatus();
		     
	    	//display the stat choice
	    	System.out.println("1) +10 herouhHealth");
	    	System.out.println("2) +1 Attack");
	    	System.out.println("3) +3 Magic");
		    System.out.printf("You have %d coins to spend: ", points);
	        int choice = input.nextInt();
	        System.out.println();
	        if(choice==1){
	        	heroHealth+=HP;//heroHealth
	        }
	     	else if(choice==2){
	     		heroAP+=AP;//attack power
	     	}
	     	else if(choice==3){
	     		magicPow+=MP; //magic power
	     	}


	     	else {
	     		System.out.println("Please enter a valid option!\n");
		            points++;
	     	}
		    points--;   
	    }
	 
	}

	public static int createMonster(){
	        Random randomGenerate = new Random();
	        
	        int randomMon = randomGenerate.nextInt(3);
	        int randomHP=randomGenerate.nextInt(25);
	        
	        monHealthArray[0]= 75+randomHP; //Health for Goblin
	        monHealthArray[1]=100+randomHP;//HEalth for Orc
	        monHealthArray[2]= 150+randomGenerate.nextInt(49);//Health for Troll

	        monXpArray[0]= randomGenerate.nextInt(3)+2; //xp for Goblin
	        monXpArray[1]=randomGenerate.nextInt(5)+3;//xp for Orc
	        monXpArray[2]= randomGenerate.nextInt(10)+4;//xp for Troll
	        
	        monApArray[0] =randomGenerate.nextInt(3)+10;//attack for GOblin
	        monApArray[1] =randomGenerate.nextInt(5)+10;//attack for Orc
	        monApArray[2] =randomGenerate.nextInt(7)+10;//attack for Troll


	        System.out.printf("\nYou have encountered a %s!\n",monNameArray[randomMon]);
	        return randomMon;
	    }
	private static void reportStatus( int monNumber){
		    
		    	/*Report Combat Starts*/
		    	//Print Monster’s name
		    	//Print Monster's attack power
		    	//Print Monster’sheath
		    	//Print the Player’s heath
		    	//Print the Player's attack power
		    	//Print the Player’s magic points

		    	System.out.printf("%s: HP: %d, AP: %d\n", monNameArray[monNumber], monHealthArray[monNumber], monApArray[monNumber]);
	            printHeroStatus();
		    	System.out.println();
			
		}


}